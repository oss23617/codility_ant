import java.util.*;

// you can write to stdout for debugging purposes, e.g.
// System.out.println("this is a debug message");

class Solution {

  //    private static ArrayList<String> stack;
  //    private static ArrayList<String> inputArray;
  private static List<String> stack;
  private static List<String> inputArray;


  Solution () {
    stack = new ArrayList<String>(0);
    inputArray = new ArrayList<String>(0);
  }
  //  private static Character[] charObjectArray;


  public static Integer solution(String[] input) {

    //System.out.println (S);

    //inputArray = new ArrayList<String>(Arrays.asList(S.split(" ")));
    inputArray = new ArrayList<String> (Arrays.asList(input));
    //stack = new ArrayList<String>(inputArray.size());
    //charObjectArray = ArrayUtils.toObject(charArray);
    while (inputArray.size()!=0) {

      if (!(inputArray.get(0).equals("+") || inputArray.get(0).equals("*") || inputArray.get(0).equals("-") || inputArray.get(0).equals("/"))){
        //System.out.println("inputArray: " + Arrays.toString(inputArray.toArray()));
        push (inputArray.get(0).toString());
        inputArray.remove(0);
        //            charObjectArray.delete(charObjectArray.size());
      } else {
        //push (inputArray.get(0).toString());
        //System.out.println("inputArray: " + Arrays.toString(inputArray.toArray()));
        //System.out.println("stack: " + Arrays.toString(stack.toArray()));
        try {
          pop(inputArray.get(0));
          inputArray.remove(0);
        } catch (StackEmptyException ex) {
          System.out.println(ex.toString());
          inputArray.remove(0);
        }
        //System.out.println("stack: " + Arrays.toString(stack.toArray()));
      }

    }
    if (stack.size()>0){
        return Integer.parseInt(stack.get(0));
    } else{
      return null;
    }
  }

  //Query:
  protected static String top() throws StackEmptyException{
    try {
      return stack.get(stack.size()-1);
    } catch (IndexOutOfBoundsException ex) {
      //System.out.println(ex.toString());
      throw new StackEmptyException();
    }

  }
  //Query:
  protected static int size() {
    return stack.size();
  }

  //Command:
  protected static void pop  (String operator) throws StackEmptyException {
  try {

    if (operator.equals("+")) {
      stack.set(stack.size()-2, new Integer((Integer.parseInt(stack.get(stack.size()-2)) + Integer.parseInt(stack.get(stack.size()-1)))).toString());
      stack.remove(stack.size()-1);
    }
    else if (operator.equals("-")) {
      stack.set(stack.size()-2, new Integer((Integer.parseInt(stack.get(stack.size()-2)) - Integer.parseInt(stack.get(stack.size()-1)))).toString());
      stack.remove(stack.size()-1);
    }
    else if (operator.equals("*")) {
      stack.set(stack.size()-2, new Integer((Integer.parseInt(stack.get(stack.size()-2)) * Integer.parseInt(stack.get(stack.size()-1)))).toString());
      stack.remove(stack.size()-1);
    } else if (operator.equals("/")) {
      stack.set(stack.size()-2, new Integer((Integer.parseInt(stack.get(stack.size()-2)) / Integer.parseInt(stack.get(stack.size()-1)))).toString());
      stack.remove(stack.size()-1);
    }

  } catch (IndexOutOfBoundsException ex) {
    //System.out.println(ex.toString());
    throw new StackEmptyException();
  }

  }

  //Command:
  protected static void push (String s) {

    stack.add(s);

  }

  public static void main (String[] args ) {
    Solution solution = new Solution();


    try {


  	  Integer result;
  	  String[] input = args;
      //System.out.println ("input: " + input);
      //for(int i = 0; i < args.length; i++) {
      //      System.out.println(args[i]);
      //  }
  	  result = solution (input);

      if (result!=null){
  	    System.out.println (result);
      }
    }catch(ArrayIndexOutOfBoundsException ex){
      //System.out.println(ex.toString());
      System.out.println("No argument given");
    }//catch (StackEmptyException ex) {
    //  System.out.println(ex.toString());
    //}

  }

}

/**
 * My custom exception class.
 */
class StackEmptyException extends Exception
{
	 //Parameterless Constructor
      public StackEmptyException() {}


  public StackEmptyException(String message)
  {
    super(message);
  }
}

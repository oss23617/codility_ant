import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MyTests {

  private Solution tester;

  @Before
  public void setup(){
    tester = new Solution(); // Solution is tested
  }


  @Test
  public void empty() {


    // assert statements
    assertEquals("Solution.size()!=0", 0, tester.size());

  }

  @Test
  public void top_empty_exception() {
    String s = "";

    // assert statements
    assertEquals("Solution.size()!=0", 0, tester.size());

    try {
      s = tester.top();
    } catch (StackEmptyException ex) {
      System.out.println(ex.toString());
    }

    assertEquals("s is not empty", "", s);

  }

  @Test
  public void push() {

    // assert statements
    tester.push("5");
    assertEquals("Solution.size()!=1", 1, tester.size());
    try {
      assertEquals("Solution.top()!=5", 5, Integer.parseInt(tester.top()));
    } catch (StackEmptyException ex) {
      System.out.println(ex.toString());
    }

  }

  @Test
  public void push_pop() {


    // assert statements
    tester.push("5");
    assertEquals("Solution.size()!=1", 1, tester.size());
    try {
      assertEquals("Solution.top()!=5", 5, Integer.parseInt(tester.top()));
    } catch (StackEmptyException ex) {
      System.out.println(ex.toString());
    }

    tester.push("2");
    assertEquals("Solution.size()!=2", 2, tester.size());
    try {
      assertEquals("Solution.top()!=2", 2, Integer.parseInt(tester.top()));

      tester.pop("*");
      Integer result = Integer.parseInt(tester.top());
      //assertEquals("Solution.stack.size()!=1", 1, tester.stack.size());
      assertEquals("Solution.top()!=10", 10, (int)result);
    } catch (StackEmptyException ex) {
      System.out.println(ex.toString());
    }



  }

  @Test
  public void solve2(){

    String[] input = {"12", "11", "+", "15", "27", "-", "*"};
    Integer result = tester.solution (input);

    assertEquals("Solution.solution("+ input +")!=-276", -276, (int)result);

  }

  @Test
  public void solve(){

    String[] input = {"3", "5", "+", "7", "2", "-", "*"};
    Integer result = tester.solution (input);


    assertEquals("Solution.solution("+ input +")!=40", 40, (int)result);

  }

  @After
  public void tearDown(){

  }

}
